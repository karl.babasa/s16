console.log("hello world!");

// repeatition Control Structures

/*
	3 types of looping contructs
		1. While Loop
		2. Do-While Loop
		3. For Loop
*/

/*
	While Loop
		-Syntax
			while(expression/condition) {
				statement/s;
			}
*/

let count = 1;

while(count !== 6) {
	console.log("While: " + count);
	count++; //count = count - 1, count =-1;

}

/*Output
	While: 1
	While: 2
	While: 3
	While: 4
	While: 5

*/

/*
	Do While Loop
		Syntax:
			do {
				statement;
			} while(expression/condition)
*/

let num1 = 1;

do{
	console.log("Do-While: " + num1);
	num1++;
} while (num1 < 6);



let num2 = Number(prompt("Give me a Number"));

do {
	console.log("Do-While: " + num2);
	num2 += 1;
} while (num2 < 10)


/*
	For loop
	Syntax:
		for(initialization; expression/condition; finalExpression) {
			statement;
		}
*/

console.log("For Loop sample");

for(let num3 = 1; num3 <=5; num3++){
	console.log(num3);
}

let newNum = Number(prompt("Enter a Number"));

for(let num4 = newNum; num4 < 10; num4++) {
	console.log(num4);
}

let numberInput = Number(prompt("Enter a Number"));

for(let numOne = 1; numberInput >= numOne; numOne++) {
	console.log(numOne);
}

console.log("String Iteration");

let myString = "Juan";
console.log(myString.length);

console.log(myString[0]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Displaying individual letters in a string");
// myString.lenght == 4
for(let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}

console.log("Vowel");

let myName = "AloNzO";

for (let i = 0; i < myName.length; i++){
	// if the character of the name is a vowel, instead of displaying the character, display number 3.
	if (
			myName[i].toLowerCase() == 'a' ||
			myName[i].toLowerCase() == 'e' ||
			myName[i].toLowerCase() == 'i' ||
			myName[i].toLowerCase() == 'o' ||
			myName[i].toLowerCase() == 'u'
		){
			console.log(3);
	}
	else{
			console.log(myName[i]);
	}
}

console.log("Comtinue and Break");

/*
	Continue and Break statements
		- Continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statents in a code block.
		- Break statement is used to terminante the current loop once a match has neen found
*/

for (let count = 0; count <= 20; count++){
	if (count % 2 == 0) {
		continue;
	}

	console.log("Continue and Break:" + count);

	if(count > 10) {
		break;
	}
}



